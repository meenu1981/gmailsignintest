package sanitytests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class frm1 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.gecko.driver","P:\\Selenium\\geckodriver-v0.11.1-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.get("http://seleniumhq.github.io/selenium/docs/api/java/index.html");
		driver.switchTo().frame(2);
		driver.findElement(By.linkText("com.thoughtworks.selenium")).click();
		Thread.sleep(3000);
		//Switch from 3rd frame to Top window
		driver.switchTo().defaultContent();
		Thread.sleep(3000);
		//Switch to 1st frame
		driver.switchTo().frame(0);
		driver.findElement(By.linkText("org.openqa.selenium")).click();


}
}