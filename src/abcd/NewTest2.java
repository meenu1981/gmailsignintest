package abcd;

import org.testng.annotations.Test;

public class NewTest2 {
	@Test(groups = {"sanity", "regression"})
	public void login(){
	System.out.println("Login Successful");
	}
	@Test (groups = {"sanity"}) 
	public void fundTransfer(){
	System.out.println("Fund Transfer Successful");
	}
	@Test(groups = {"sanity"})
	public void search(){
	System.out.println("Search Successful");
	}
	@Test (groups = {"regression"})
	public void advancedSearch(){
	System.out.println("Advanced Search Successful");
	}
	@Test(groups = {"regression"})
	public void prePaidRecharge(){
	System.out.println("PrePaid Recharge Successful");
	}
	@Test(groups = {"regression"})
	public void billPayments(){
	System.out.println("Bill Payments Successful");
	}
	@Test(groups = {"sanity", "regression"})
	public void logout(){
	System.out.println("Logout Successful");
	}
	}

