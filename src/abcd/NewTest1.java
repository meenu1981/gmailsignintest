package abcd;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class NewTest1 {
	@BeforeTest
	public void login(){
	System.out.println("Login Successful");
	}
	@AfterTest
	public void logout(){
	System.out.println("Logout Successful");
	}
	@Test (priority = 1)
	public void deleteVendor(){
	System.out.println("Delete Vendor Successful"); 
	}
	@Test(priority = 2)
	public void deleteProduct(){
	System.out.println("Delete Product Successful"); 
	}
	@Test(priority = 3)
	public void deleteCurrency(){
	System.out.println("Delete Currency Successful"); 
	}
	}

